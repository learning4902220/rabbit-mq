package uz.rabbit.rabbitmq.http;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import uz.rabbit.rabbitmq.domain.Person;

@Service
@RequiredArgsConstructor
public class MessageService {

    private final AmqpTemplate amqpTemplate;

    @RabbitListener(queues = "Test")
    public void readMassage(Person person) {
        System.out.println("Read from Queue: " + person);
    }

    public String publishMessage(Person person) {
        amqpTemplate.convertAndSend("Test", person);
        return "OK";
    }
}
