package uz.rabbit.rabbitmq.domain;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Person {

    private Long id;
    private String name;
    private String age;
}
