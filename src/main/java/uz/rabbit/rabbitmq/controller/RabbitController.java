package uz.rabbit.rabbitmq.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.rabbit.rabbitmq.domain.Person;
import uz.rabbit.rabbitmq.http.MessageService;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class RabbitController {
    private final MessageService messageService;

    @PostMapping("/test")
    public String putToQueue(@RequestBody Person person) {
        return messageService.publishMessage(person);
    }
}
